﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DependencyRefactor
{
    class Almacen:IAuditable
    {
        //YA NO HAY NECESIDAD DE DAR ACCESO A LA LISTA
        private List<Producto> inventario;

        //Constructor
        public Almacen()
        {
            inventario = new List<Producto>();
        }

        public void AdicionaProducto(Producto pProducto)
        {
            inventario.Add(pProducto);
            Console.WriteLine("Adicionamos {0}", pProducto.Nombre);
        }
        //implementacion de la interfaz, hacemos la busqueda flexible
        public IEnumerable<Producto> ObtenProductos(int pTipo)
        {
            List<Producto> encontrados = new List<Producto>();

            foreach(Producto p in inventario)
            {
                if (p.Tipo == pTipo)
                    encontrados.Add(p);
            }
            return encontrados;
        }
    }
}
