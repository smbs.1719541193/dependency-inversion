﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DependencyRefactor
{
    interface IAuditable
    {
        //Metodo o Comportamiento
        IEnumerable<Producto> ObtenProductos(int pTipo);
    }
}
