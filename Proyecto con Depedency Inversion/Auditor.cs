﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DependencyRefactor
{
    class Auditor
    {
        private IAuditable miAlmacen;

        //Ahora el auditor ya no está fuertemente acoplado con el almacén
        //Constructor
        public Auditor(IAuditable pAlmacen)//Puede trabajar con cualquiera que implemente IAuditable
        {
            miAlmacen = pAlmacen;
        }
        //Metodo
        public double totalesAlimentos()
        {
            double total = 0;

            IEnumerable<Producto> listado = miAlmacen.ObtenProductos(0);//Desacoplados de almacen, listado enviado de Almacen
            
            foreach (Producto p in listado)
            {
                Console.WriteLine(p);
                total += p.Costo;
            }
            return total;
        }
    }
}
