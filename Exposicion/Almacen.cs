﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DependencyInversion
{
    class Almacen
    {
        private List<Producto> inventario; 
        //Nos vemos forzados a crear la propiedad para que se pueda contar el inventario
        //Al ser una propiedad de tipo List forzamos a Auditor a trabajar con List
        public List<Producto> Inventario { get => inventario; set => inventario = value; }

        //Constructor
        public Almacen()
        {
            inventario = new List<Producto>();
        }
        
        public void AdicionaProducto(Producto pProducto)
        {
            inventario.Add(pProducto);
            Console.WriteLine("Adicionamos {0}", pProducto.Nombre);
        }
    }

}
